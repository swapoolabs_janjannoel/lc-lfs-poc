# Git LFS Proof-of-Concept

### Installation
Follow the instructions on Git LFS installation [here](https://github.com/git-lfs/git-lfs/wiki/Installation) before cloning the repository.

### Authentication
Use your Bitbucket credentials when asked for authentication. If you can access this repository, then you should be able to access the Git LFS server as well.

### Usage
[See Git LFS documentation](https://github.com/git-lfs/git-lfs/blob/master/README.md#example-usage)

### Git LFS url
https://y3mt6zz1i3.execute-api.us-east-1.amazonaws.com/lfs